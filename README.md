Forked from https://github.com/wobbals/dogbone

open tok subscriber project

## Build Instructions:

This requires nodejs 4.x.

You need to npm restore twice.

```bash
npm restore

cd ./node_modules/@opentok/neutrino-util

npm restore
```