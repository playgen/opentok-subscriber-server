var zmq = require('zeromq');
var watson = require('./watson.js');

const url = 'ipc:///tmp/opentok-node-subscriber';

var controller = zmq.socket('push');
controller.bindSync(url);

var receiver = zmq.socket('pull');
receiver.connect(url);

module.exports.sendControlMessage = function(message) {
  controller.send(message);
};

receiver.on('message', function(streamId, path){
  console.log(`received ${streamId}, ${path} from pup`);
  watson.recognizeStreamPath(streamId, path);
});
