const credentials = require('../credentials.js');
const JWT = require('jsonwebtoken');
const request = require('request');
const http = require('http');
const assert = require('assert');
const promise = require('promise');
const child_process = require('child_process');
const httpShutdown = require('http-shutdown');
const opentok = require('opentok');
const uuid = require('node-uuid');

const launchBrowserAddress = "localhost";
const launchBrowserPort = 53847;

describe("OpenTok Node Subscriber", function() {
    it("should record streams", function(done){
        const OT = new opentok(credentials.openTok.apiKey, credentials.openTok.secret);

        generateSessionAsync(OT)
            .catch(error => done(error))
            .then(sessionId => {

                // Start Subscriber Server
                var subscriberToken = OT.generateToken(sessionId, {
                    role: "subscriber",
                    clientId: "subscriber-server"
                });

                credentials.openTok.sessionId = sessionId;                
                credentials.openTok.token = subscriberToken;                
                var subscriberServer = require('../server.js');

                createPageServer(sessionId, OT)
                    .then(server => {
                        // Open browsers for video session
                        var browser1 = child_process.exec("explorer.exe http://" + launchBrowserAddress + ":" + launchBrowserPort);
                        var browser2 = child_process.exec("explorer.exe http://" + launchBrowserAddress + ":" + launchBrowserPort);

                        waitForSeconds(30)
                            .then(() => {

                                // Assert videos exist

                                // Cleanup   
                                browser1.kill();
                                browser2.kill();                                
                                server.shutdown(done);
                                subscriberServer.disconnect();                              
                            });              
                    });  
            });
    });
});

function generateSessionAsync(OT) {
    return new Promise((resolve, reject) => {
        OT.createSession({mediaMode: "routed"}, (error, session) => {
            if(error) {
                reject(error);
            } else {
                resolve(session.sessionId);
            }
        });
    });
}

function createPageServer(sessionId, OT) {
    return new Promise(resolve => {
        
        var server = http.createServer((request, response) => {
            var html = buildHtml(sessionId, OT);

            response.writeHead(200, {
                'Content-Type': 'text/html',
                'Content-Length': html.length,
                'Expires': new Date().toUTCString()
            });

            response.end(html);
        });
        
        server = httpShutdown(server);

        server.listen(launchBrowserPort, launchBrowserAddress, () => resolve(server));
    });
}

function buildHtml(sessionId, OT) {
    var token = OT.generateToken(sessionId, {
        role: "publisher",
        clientId: uuid.v4()
    });

    var header = '<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>';
    
    var body = '<div id="subscriber" style="position: absolute;height: 100%;"></div>'
        + '<div id="publisher" style="position: absolute;"></div>'
        + '<script>'
        + 'var session = OT.initSession(' + credentials.openTok.apiKey + ', "' + sessionId + '");'
        + ''
        + 'session.on("streamCreated", function(event) {'
        + '    session.subscribe(event.stream, "subscriber", {'
        + '        width: "100%",'
        + '        height: "100%"'
        + '    });'
        + '});'
        + ''
        + 'session.connect("' + token + '", function() {'
        + '    session.publish(OT.initPublisher("publisher"));'
        +'});'
        + '</script>';

    return '<!DOCTYPE html><html><header>' + header + '</header><body>' + body + '</body></html>';
};

function waitForSeconds(seconds) {
    return new Promise(resolve => setTimeout(resolve, seconds * 1000));
}